<?php

return [
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=localhost;dbname=u0551855_np',
            'username' => 'u0551855_np3',
            'password' => 'F5w4I5b0',
            'tablePrefix' => 'np_',
        ],
        'mailer' => [
            'useFileTransport' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];