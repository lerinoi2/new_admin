<?php

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'name' => 'Novoe Pokolenie',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'main/default/index',
                '<_a:error>' => 'main/default/<_a>',
                '<_a:(login|logout)>' => 'main/default/<_a>',

                '/receipt' => 'book/default/up_create',
                '/edit/up/<id:\d+>' => 'book/default/up_edit',
                '/edit/up/<id:\d+>/print' => 'book/default/print_up',

                '/recount' => 'book/default/recount',

                '/expense' => 'book/default/down_create',
                '/edit/down/<id:\d+>' => 'book/default/down_edit',
                '/edit/down/<id:\d+>/print' => 'book/default/print_down',

                '/getCash' => 'book/default/get_cash',
                '/book_print' => 'book/default/book_print',

                '/general_print' => 'book/default/general_print',

                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
                '<_m:[\w\-]+>' => '<_m>/default/index',
                '<_m:[\w\-]+>/<_c:[\w\-]+>' => '<_m>/<_c>/index',
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'cache' => [
            'class' => 'yii\caching\DummyCache',
        ],
        'log' => [
            'class' => 'yii\log\Dispatcher',
        ],
    ],
    'params' => $params,
];