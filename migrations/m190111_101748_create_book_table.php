<?php

use yii\db\Migration;

/**
 * Handles the creation of table `book`.
 */
class m190111_101748_create_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%book}}', [
            'id' => $this->primaryKey(),
            'create_date' => $this->string()->notNull(),
            'FIO' => $this->string()->notNull(),
            'type' => $this->integer(1)->notNull(),
            'purpose' => $this->string()->notNull(),
            'doc' => $this->string()->notNull(),
            'summ' => $this->float(10.2)->notNull(),
            'username' => $this->string()->notNull(),
            'book_type' => $this->integer(1)->notNull(),
            'number' => $this->integer(1)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%book}}');
    }
}
