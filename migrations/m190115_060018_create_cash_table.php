<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cash`.
 */
class m190115_060018_create_cash_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cash}}', [
            'id' => $this->primaryKey(),
            'day' => $this->string()->notNull(),
            'start_day' => $this->string()->notNull(),
            'up_day' => $this->string()->notNull(),
            'down_day' => $this->string()->notNull(),
            'end_day' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cash}}');
    }
}
