<?php

namespace app\modules\book\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use app\modules\book\models\Book;
use app\modules\book\models\Cash;

/**
 * Default controller for the `book` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->response->redirect(['login']);
            return false;
        }
        return true;
    }

    public function actionUp_create()
    {
        $model = new Book;
        $user = Yii::$app->user->identity->username;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($user != 'Valeriy') {
                $d = date('Y-m-d', $model->create_date);
                $cash = Cash::find()->where(['day' => $d])->one();
                if ($cash) {
                    $up_day = $model->summ + $cash->up_day;
                    $end_day = ($cash->start_day + $up_day) - $cash->down_day;
                    $cash->up_day = (string)$up_day;
                    $cash->end_day = (string)$end_day;
                    $cash->save();

                    //пересчет для остальных дат
                    $last_cash = Cash::find()->orderBy('id DESC')->one();
                    $id = $cash->id + 1;
                    for ($i = $id; $i <= $last_cash->id; $i++) {
                        $item = Cash::find()->where(['id' => $i])->one();
                        $start_day = $item->start_day + $model->summ;
                        $end_day = $item->end_day - $model->summ;
                        $item->start_day = (string)$start_day;
                        $item->end_day = (string)$end_day;
                        $item->save();
                    }
                }
                else {
                    $last_cash = Cash::find()->orderBy('id DESC')->one();
                    $cash = new Cash();
                    $cash->day = $d;
                    $cash->start_day = $last_cash->end_day;
                    $cash->up_day = $model->summ;
                    $cash->down_day = '0';
                    $end_day = $last_cash->end_day + $model->summ;
                    $cash->end_day = (string)$end_day;
                    $cash->save();
                }
            }
            return $this->redirect('/edit/up/' . $model->id);
        }
        else {
            $arrTypes = Book::getType();
            $today = Yii::$app->formatter->asDate('now', 'php:d.m.Y');
            $newId = Book::find()->where(['book_type' => '0'])->orderBy('number DESC')->one();
            $user = Yii::$app->user->identity;
            return $this->render('up_create', ['types' => $arrTypes, 'date' => $today, 'id' => $newId->number + 1, 'user' => $user->username, 'model' => $model]);
        }
    }

    public function actionUp_edit($id)
    {
        $item = Book::find()->where(['id' => $id])->one();
        $type = Book::getType();
        return $this->render('up_edit', ['item' => $item, 'type' => $type, 'id' => $id]);
    }

    public function actionDown_create()
    {
        $model = new Book;
        $user = Yii::$app->user->identity->username;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($user != 'Valeriy') {
                $d = date('Y-m-d', $model->create_date);
                $cash = Cash::find()->where(['day' => $d])->one();
                if ($cash) {
                    $down_day = $model->summ + $cash->down_day;
                    $end_day = ($cash->start_day + $cash->up_day) - $down_day;
                    $cash->down_day = (string)$down_day;
                    $cash->end_day = (string)$end_day;
                    $cash->save();

                    $last_cash = Cash::find()->orderBy('id DESC')->one();
                    $id = $cash->id + 1;
                    for ($i = $id; $i <= $last_cash->id; $i++) {
                        $item = Cash::find()->where(['id' => $i])->one();
                        $start_day = $item->start_day - $model->summ;
                        $end_day = $item->end_day - $model->summ;
                        $item->start_day = (string)$start_day;
                        $item->end_day = (string)$end_day;
                        $item->save();
                    }
                }
                else {
                    $last_cash = Cash::find()->orderBy('id DESC')->one();
                    $cash = new Cash();
                    $cash->day = $d;
                    $cash->start_day = $last_cash->end_day;
                    $cash->end_day = (string)($last_cash->end_day - $model->summ);
                    $cash->up_day = '0';
                    $cash->down_day = (string)$model->summ;
                    $cash->save();
                }
            }
            return $this->redirect('/edit/down/' . $model->id);
        }
        else {
            $type = Book::getType2();
            $user = Yii::$app->user->identity;
            $today = Yii::$app->formatter->asDate('now', 'php:d.m.Y');
            $newId = Book::find()->where(['book_type' => '1'])->orderBy('number DESC')->one()->number;
            return $this->render('down_create', ['model' => $model, 'type' => $type, 'user' => $user->username, 'date' => $today, 'number' => $newId + 1]);
        }
    }

    public function actionDown_edit($id)
    {
        $item = Book::find()->where(['id' => $id])->one();
        $type = Book::getType2();
        return $this->render('down_edit', ['item' => $item, 'type' => $type, 'id' => $id]);
    }

    public function actionPrint_up($id)
    {
        $filename = "excel/up.xls";
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $spreadsheet = $reader->load($filename);
        $item = Book::find()->where(['id' => $id])->one();
        $summ = explode('.', $item->summ);
        $spreadsheet->getActiveSheet()->setCellValue('F12', $item->number); //номер док
        $spreadsheet->getActiveSheet()->setCellValue('H12', date('d.m.Y', $item->create_date)); //Дата
        $spreadsheet->getActiveSheet()->setCellValue('G18', $item->summ); //Сумма
        $spreadsheet->getActiveSheet()->setCellValue('C20', $item->FIO); //От
        $spreadsheet->getActiveSheet()->setCellValue('B23', $item->purpose); //Основание
        $spreadsheet->getActiveSheet()->setCellValue('B26', $this->num2str($item->summ)); //Сумма прописью
        $spreadsheet->getActiveSheet()->setCellValue('L5', 'к приходному кассовому ордеру № ' . $id); //к приходному кассовому ордеру №
        $spreadsheet->getActiveSheet()->setCellValue('M6', $this->month($item->create_date)); //дата на русском месяц
        $spreadsheet->getActiveSheet()->setCellValue('L9', $item->FIO); //Принято от
        $spreadsheet->getActiveSheet()->setCellValue('L14', $item->purpose); //Основание
        $spreadsheet->getActiveSheet()->setCellValue('M18', $summ[0] . " руб. " . $summ[1] . " коп."); //Сумма
        $spreadsheet->getActiveSheet()->setCellValue('L20', $this->num2str($item->summ)); //Сумма
        $spreadsheet->getActiveSheet()->setCellValue('M28', $this->month($item->create_date)); //Дата
        $writer = new Xls($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Приход.xls"');

        return \Yii::$app->response->sendFile($writer->save('php://output'));
    }

    function num2str($num)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array( // Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1) $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = $this->morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        $out[] = $kop . ' ' . $this->morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;
        return $f5;
    }

    function month($date)
    {
        $arr = [
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря'
        ];
        $month = date('n', $date) - 1;
        $day = date('d', $date);
        $year = date('Y', $date);
        return $day . " " . $arr[$month] . " " . $year;
    }

    public function actionPrint_down($id)
    {
        $filename = "excel/down.xls";
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $spreadsheet = $reader->load($filename);
        $item = Book::find()->where(['id' => $id])->one();
        $spreadsheet->getActiveSheet()->setCellValue('G9', $item->number); //номер док
        $spreadsheet->getActiveSheet()->setCellValue('I9', date('d.m.Y', $item->create_date)); //Дата
        $spreadsheet->getActiveSheet()->setCellValue('G13', $item->summ); //Сумма
        $spreadsheet->getActiveSheet()->setCellValue('C14', $item->FIO); //Выдать
        $spreadsheet->getActiveSheet()->setCellValue('C16', $item->purpose); //Основание
        $spreadsheet->getActiveSheet()->setCellValue('C17', $this->num2str($item->summ)); //Сумма прописью
        $spreadsheet->getActiveSheet()->setCellValue('C20', $item->doc); //Приложение
        $spreadsheet->getActiveSheet()->setCellValue('C26', $this->num2str($item->summ)); //Получил
        $spreadsheet->getActiveSheet()->setCellValue('B29', $this->month($item->create_date)); //Дата
        $writer = new Xls($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Расход.xls"');

        return \Yii::$app->response->sendFile($writer->save('php://output'));
    }

    public function actionGet_cash($start, $end)
    {
        $start = explode('.', $start);
        $end = explode('.', $end);
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM np_cash where CAST(day AS DATE) BETWEEN '" . $start[2] . '-' . $start[1] . '-' . $start[0] . "' AND '" . $end[2] . '-' . $end[1] . '-' . $end[0] . "'");
        $result = $command->queryAll();

        $up = 0;
        $down = 0;

        foreach ($result as $key => $value) {
            $up += $value['up_day'];
            $down += $value['down_day'];
        }

        $start_sum = $result[0]["start_day"];
        $end_sum = array_pop($result)["end_day"];

        $data = array(
            'start' => $start_sum,
            'up' => $up,
            'down' => $down,
            'end' => $end_sum,
        );

        return json_encode($data);
    }

    public function actionBook_print($ids, $start, $end, $star_day, $end_day)
    {
        $res_p = 0;
        $res_r = 0;
        $filename = "excel/cash.xls";
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $spreadsheet = $reader->load($filename);
        if ($start == $end) {
            $date = 'КАССА за ' . $start;
        }
        else {
            $date = 'КАССА с ' . $start . ' по ' . $end;
        }
        if ($star_day >= 0) {
            $s_day_p = $star_day;
            $s_day_r = 'X';
        }
        else {
            $s_day_p = 'X';
            $s_day_r = $star_day;
        }

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM np_book where (create_date BETWEEN '" . strtotime($start) . "' AND '" . strtotime($end) . "')  and (username != 'Valeriy')");
        $result = $command->queryAll();

        foreach ($result as $val) {
            if ($val['book_type'] == 0) {
                $who = 'Принято от ' . $val['FIO'];
                $res_p += $val['summ'];
            }
            else {
                $who = 'Выдано ' . $val['FIO'];
                $res_r += $val['summ'];
            }

            $data[] = array(
                'number' => $val['number'],
                'who' => $who,
                'sum' => $val['summ'],
                'type' => $val['book_type']
            );
        }

        $spreadsheet->getActiveSheet()->setCellValue('A1', $date); //Дата
        $spreadsheet->getActiveSheet()->setCellValue('F5', $s_day_p); //Остаток на начало дня Приход
        $spreadsheet->getActiveSheet()->setCellValue('G5', $s_day_r); //Остаток на начало дня Расход
        foreach ($data as $key => $value) {
            $ind = $key + 6;
            $next_ind = $ind + 1;
            $spreadsheet->getActiveSheet()->setCellValue('A' . $ind, $value['number']); //Номер документв
            $spreadsheet->getActiveSheet()->setCellValue('B' . $ind, $value['who']); //Кто
            if ($value['type'] == 0) {
                $spreadsheet->getActiveSheet()->setCellValue('F' . $ind, $value['sum']); //Приход
            }
            else {
                $spreadsheet->getActiveSheet()->setCellValue('G' . $ind, $value['sum']); //Расход
            }
            if (isset($data[$key + 1])) {
                $spreadsheet->getActiveSheet()->insertNewRowBefore($next_ind, 1);
                $spreadsheet->getActiveSheet()->mergeCells("B" . $next_ind . ":D" . $next_ind);
            }
            else {
                $spreadsheet->getActiveSheet()->setCellValue('F' . $next_ind, $res_p); //Итого за день приход
                $spreadsheet->getActiveSheet()->setCellValue('G' . $next_ind, $res_r); //Итого за день расход

                if ($end_day >= 0) {
                    $spreadsheet->getActiveSheet()->setCellValue('F' . ($next_ind + 1), $end_day); //Остаток на конец дня приход
                    $spreadsheet->getActiveSheet()->setCellValue('G' . ($next_ind + 1), "X"); //Остаток на конец  дня расход
                }
                else {
                    $spreadsheet->getActiveSheet()->setCellValue('F' . ($next_ind + 1), "X"); //Остаток на конец дня приход
                    $spreadsheet->getActiveSheet()->setCellValue('G' . ($next_ind + 1), $end_day); //Остаток на конец  дня расход
                }
                $spreadsheet->getActiveSheet()->setCellValue('G' . ($next_ind + 2), "X"); //Остаток на конец  дня зп
            }
        }


        $writer = new Xls($spreadsheet);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=Кассовая книга.xls');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        return \Yii::$app->response->sendFile($writer->save('php://output'));
    }

    public function actionGeneral_print()
    {
        $request = Yii::$app->request;
        $get = $request->get();
        if ($get) {
            $get = $get["BookSearch"];
        }

        $filename = "excel/general.xls";
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $spreadsheet = $reader->load($filename);

        $r = '';
        $a1 = '';
        $a2 = '';
        $a3 = '';
        $a4 = '';
        $a5 = '';
        $a6 = '';
        $a7 = '';
        $a8 = '';

        if (!empty($get['date_from']) or !empty($get['date_to']) or !empty($get['number']) or !empty($get['book_type']) or !empty($get['FIO']) or !empty($get['type']) or !empty($get['purpose']) or !empty($get['summ']) or !empty($get['username'])) {
            $r .= ' where ';
        }

        if (!empty($get['date_from']) and !empty($get['date_to'])) {
            $a1 = "(create_date BETWEEN '" . strtotime($get['date_from']) . "' AND '" . strtotime($get['date_to']) . "') and ";
        }

        if (!empty($get['number'])) {
            $a2 = "(number = '" . $get['number'] . "') and";
        }

        if (!empty($get['book_type'])) {
            $a3 = "(book_type = '" . $get['book_type'] . "') and";
        }

        if (!empty($get['FIO'])) {
            $a4 = "(fio LIKE '%" . $get['FIO'] . "%') and";
        }

        if (!empty($get['type'])) {
            $a5 = "(type = '" . $get['type'] . "') and";
        }

        if (!empty($get['purpose'])) {
            $a6 = "(purpose LIKE '%" . $get['purpose'] . "%') and";
        }

        if (!empty($get['summ'])) {
            $a7 = "(summ  = '" . $get['summ'] . "') and";
        }

        if (!empty($get['username'])) {
            $a8 = "(username  = '" . $get['username'] . "') and";
        }

        $r .= $a1 . $a2 . $a3 . $a4 . $a5 . $a6 . $a7 . $a8;

        $str = explode(' ', "SELECT * FROM np_book " . $r);
        if (array_pop($str) == 'and') {
//            unset(array_pop($str));
        }

        $qwe = '';
        foreach ($str as $s) {
            $qwe .= $s . " ";
        }

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($qwe ?: '"SELECT * FROM np_book');
        $result = $command->queryAll();

        foreach ($result as $key => $value) {
            $index = $key + 1;
            $next_index = $index + 1;
            $arrTypes = Book::getTypes();
            $type = $arrTypes[$value['type']];

            $spreadsheet->getActiveSheet()->setCellValue('A' . $next_index, $index);
            $spreadsheet->getActiveSheet()->setCellValue('B' . $next_index, date('d.m.Y H:i:s', $value['create_date']));
            $spreadsheet->getActiveSheet()->setCellValue('C' . $next_index, $value['number']);
            $spreadsheet->getActiveSheet()->setCellValue('D' . $next_index, $value['book_type'] == 0 ? "Приход" : "Расход");
            $spreadsheet->getActiveSheet()->setCellValue('E' . $next_index, $value['FIO']);
            $spreadsheet->getActiveSheet()->setCellValue('F' . $next_index, $type);
            $spreadsheet->getActiveSheet()->setCellValue('G' . $next_index, $value['purpose']);
            $spreadsheet->getActiveSheet()->setCellValue('H' . $next_index, $value['summ']);
            $spreadsheet->getActiveSheet()->setCellValue('I' . $next_index, $value['username']);

            if (isset($model[$key + 2])) {
                $spreadsheet->getActiveSheet()->insertNewRowBefore(($next_index + 1), 1);
            }
        }

        $writer = new Xls($spreadsheet);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=Общий вид.xls');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');


        return \Yii::$app->response->sendFile($writer->save('php://output'));

    }

    public function actionRecount()
    {
        $cash = Cash::find()->asArray()->orderBy('id ASC')->all();
        $end_day = 0;
        foreach ($cash as $key => $item) {
            $downsALL = 0;
            $dayALL = 0;
            $day = strtotime($item['day']);
            $ups = Book::find()->where(['create_date' => $day, 'book_type' => 0, 'username' => 'Victoriya'])->orderBy('id DESC')->asArray()->all();
            $downs = Book::find()->where(['create_date' => $day, 'book_type' => 1, 'username' => 'Victoriya'])->orderBy('id DESC')->asArray()->all();
            foreach ($ups as $up) {
                $dayALL += $up['summ'];
            }
            foreach ($downs as $down) {
                $downsALL += $down['summ'];
            }


            $start = $end_day;

            $end_day = ($start + $dayALL) - $downsALL;

            $update = Cash::find()->where(['id' => $item['id']])->one();
            if ($key <= 1) {
                $update->start_day = '0';
            } else {
                $update->start_day = (string)$start;
            }
            $update->day = (string)$item['day'];
            $update->up_day = (string)$dayALL;
            $update->down_day = (string)$downsALL;
            $update->end_day = (string)$end_day;
            $update->save();
        }
    }
}
