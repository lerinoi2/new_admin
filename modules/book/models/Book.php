<?php

namespace app\modules\book\models;

use Yii;

/**
 * This is the model class for table "{{%book}}".
 *
 * @property int $id
 * @property string $create_date
 * @property string $FIO
 * @property int $type
 * @property string $purpose
 * @property string $doc
 * @property string $summ
 * @property string $username
 */
class Book extends \yii\db\ActiveRecord
{

    const TYPE_PUT = 0;
    const TYPE_MARSH = 1;

    const TYPE_BACK = 2;
    const TYPE_OTCH = 3;
    const TYPE_CURRENT = 4;
    const TYPE_BANK = 5;


    public static function getType()
    {
        return array(
            self::TYPE_PUT => 'Оплата за путевку',
            self::TYPE_MARSH => 'Оплата за маршрута выходного дня'
        );
    }

    public static function getType2()
    {
        return array(
            self::TYPE_BACK => 'Возврат',
            self::TYPE_OTCH => 'Подотчет',
            self::TYPE_CURRENT => 'Текущие расходы',
            self::TYPE_BANK => 'В банк',
        );
    }

    public static function getTypes()
    {
        return array(
            self::TYPE_PUT => 'Оплата за путевку',
            self::TYPE_MARSH => 'Оплата за маршрута выходного дня',
            self::TYPE_BACK => 'Возврат',
            self::TYPE_OTCH => 'Подотчет',
            self::TYPE_CURRENT => 'Текущие расходы',
            self::TYPE_BANK => 'В банк',
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%book}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FIO', 'type', 'purpose', 'summ', 'username'], 'required'],
            [['type', 'book_type', 'number'], 'integer'],
            [['summ'], 'double'],
            [['create_date', 'FIO', 'purpose', 'doc', 'username'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_date' => 'Дата / Время',
            'FIO' => 'ФИО',
            'type' => 'Тип платежа',
            'purpose' => 'Назначение платежа',
            'doc' => 'По документу',
            'summ' => 'Сумма',
            'username' => 'Пользователь',
            'book_type' => 'Тип',
            'number' => 'Номер платежа',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
//            if ($this->isNewRecord) {
                $this->create_date = strtotime($this->create_date);
//            }
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }
}
