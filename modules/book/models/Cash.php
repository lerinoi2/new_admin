<?php

namespace app\modules\book\models;

use Yii;

/**
 * This is the model class for table "{{%cash}}".
 *
 * @property int $id
 * @property string $day
 * @property string $start_day
 * @property string $sum_day
 * @property string $end_day
 */
class Cash extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cash}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['day'], 'required'],
            [['day', 'start_day', 'up_day', 'down_day', 'end_day'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'day' => 'Day',
            'start_day' => 'Start Day',
            'up_day' => 'Up Day',
            'down_day' => 'Down Day',
            'end_day' => 'End Day',
        ];
    }
}
