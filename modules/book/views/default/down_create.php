<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "Внести расход";
?>


<div class="down-default-index">
    <h2>Внести расход</h2>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'up-form']); ?>
                <?= $form->field($model, 'create_date')->textInput(['value' => $date]) ?>
                <?= $form->field($model, 'number')->textInput(['readonly' => true, 'value' => $number])->label('Номер платежа') ?>
                <?= $form->field($model, 'FIO')->textInput()?>
                <?= $form->field($model, 'type')->dropDownList($type)?>
                <?= $form->field($model, 'purpose') ?>
                <?= $form->field($model, 'doc') ?>
                <?= $form->field($model, 'summ') ?>
                <?= $form->field($model, "username")->hiddenInput(['value' => $user])->label(false) ?>
                <?= $form->field($model, "book_type")->hiddenInput(['value' => '1'])->label(false) ?>
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'name' => 'down-save']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
