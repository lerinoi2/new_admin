<?php

$this->title = "Внести расход";

/* @var $item object */
/* @var $type array */

?>

<div class="down-default-index">
    <h2>Внести расход</h2>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group field-down-create_date required">
                    <label class="control-label" for="down-create_date">Дата / Время</label>
                    <input type="text" id="down-create_date" class="form-control" value="<?=date('d.m.Y', $item->create_date)?>" readonly="">
                </div>
                <div class="form-group field-down-fio required">
                    <label class="control-label" for="down-fio">ФИО</label>
                    <input type="text" id="down-fio" class="form-control" readonly value="<?=$item->FIO?>">
                </div>
                <div class="form-group field-down-type required">
                    <label class="control-label" for="down-type">Тип платежа</label>
                    <input type="text" id="down-type" class="form-control" readonly value="<?=$type[$item->type]?>">
                </div>
                <div class="form-group field-down-purpose required">
                    <label class="control-label" for="down-purpose">Основание платежа</label>
                    <input type="text" id="down-purpose" class="form-control" readonly value="<?=$item->purpose?>">
                </div>
                <div class="form-group field-down-doc">
                    <label class="control-label" for="down-doc">По документу</label>
                    <input type="text" id="down-doc" class="form-control" readonly value="<?=$item->doc?>">
                </div>
                <div class="form-group field-down-summ required">
                    <label class="control-label" for="down-summ">Сумма</label>
                    <input type="text" id="down-summ" class="form-control" readonly value="<?=$item->summ?>">
                </div>
                <div class="form-group">
                    <a class="btn btn-primary down-print" href="/edit/down/<?=$id?>/print">Печать</a>
                </div>
            </div>
        </div>
    </div>
</div>



