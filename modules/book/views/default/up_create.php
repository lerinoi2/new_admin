<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "Внести приход";

/* @var $types array */
/* @var $date date */
/* @var $id int */
/* @var $user string */

?>
<div class="up-default-index">
    <h2>Внести приход</h2>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'up-form']); ?>
                <?= $form->field($model, 'create_date')->textInput(['value' => $date])->label('Дата / Время') ?>
                <?= $form->field($model, 'number')->textInput(['readonly' => true, 'value' => $id])->label('Номер платежа') ?>
                <?= $form->field($model, 'FIO')->textInput()->label('ФИО') ?>
                <?= $form->field($model, 'type')->dropDownList($types)->label('Тип платежа') ?>
                <?= $form->field($model, 'purpose')->label('Назначение платежа') ?>
                <?= $form->field($model, 'summ')->label('Сумма') ?>
                <?= $form->field($model, "username")->hiddenInput(['value' => $user])->label(false) ?>
                <?= $form->field($model, "book_type")->hiddenInput(['value' => '0'])->label(false) ?>
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'name' => 'up-save']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
