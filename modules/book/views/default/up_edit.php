<?php

$this->title = "Внести приход";

/* @var $item object */
/* @var $type array */

?>

<div class="up-default-index">
    <h2>Внести приход</h2>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group field-up-create_date required">
                    <label class="control-label" for="up-create_date">Дата / Время</label>
                    <input type="text" id="up-create_date" class="form-control"
                           value="<?=date('d.m.Y', $item->create_date)?>" readonly="">
                </div>
                <div class="form-group field-up-id">
                    <label class="control-label" for="up-id">Номер платежа</label>
                    <input type="text" id="up-id" class="form-control"  value="<?=$item->number?>" readonly="">
                </div>
                <div class="form-group field-up-fio required">
                    <label class="control-label" for="up-fio">ФИО</label>
                    <input type="text" id="up-fio" class="form-control" readonly="" value="<?=$item->FIO?>">
                </div>
                <div class="form-group field-up-type required">
                    <label class="control-label" for="up-type">Тип платежа</label>
                    <input type="text" id="up-type" class="form-control" readonly="" value="<?=$type[$item->type]?>">
                </div>
                <div class="form-group field-up-purpose required">
                    <label class="control-label" for="up-purpose">Назначение платежа</label>
                    <input type="text" id="up-purpose" class="form-control" readonly="" value="<?=$item->purpose?>">
                </div>
                <div class="form-group field-up-summ required">
                    <label class="control-label" for="up-summ">Сумма</label>
                    <input type="text" id="up-summ" class="form-control" readonly="" value="<?=$item->summ?>">
                </div>
                <div class="form-group">
                    <a class="btn btn-primary up-print" href="/edit/up/<?=$id?>/print">Печать</a>
                </div>
            </div>
        </div>
    </div>
</div>
