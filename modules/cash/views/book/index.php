<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\book\models\Book;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\cash\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Кассовая книга');
?>

<div class="col-lg-12 cash-info-hide">
    <div class="row">
        <div class="col-lg-3">
            <span>Остаток на начало периода: </span>
            <pre class="start-period"></pre>
        </div>

        <div class="col-lg-3">
            <span>Итого за период расходов: </span>
            <pre class="sum-down-period"></pre>
        </div>

        <div class="col-lg-3">
            <span>Итого за период приходов: </span>
            <pre class="sum-up-period"></pre>
        </div>

        <div class="col-lg-3">
            <span>Остаток на конец периода: </span>
            <pre class="end-period"></pre>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-right" style="margin-bottom: 20px;">
                <a class="btn btn-success book_print">Печать</a>
            </div>
        </div>
    </div>
    </div>

<div class="book-index cash">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Номер документа',
                'attribute' => 'number',
            ],
            [
                'attribute' => 'create_date',
                'value' => function ($model, $key, $index, $grid) {
                    return date('d.m.Y', $model->create_date);
                },
                'filter' => kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'attribute2' => 'date_to',
                    'type' => kartik\date\DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'weekStart' => 1,
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ],
                ]),
            ],
            [
                'label' => 'От кого',
                'attribute' => 'username',
            ],
            [
                'label' => 'Кому',
                'attribute' => 'FIO',
            ],
            [
                'attribute' => 'book_type',
                'value' => function ($model, $key, $index, $grid) {
                    if ($model->book_type == 0) {
                        $text = 'Приход';
                    } else {
                        $text = 'Расход';
                    }
                    return $text;
                },
                'filter' => array('Приход', 'Расход'),
            ],
            'summ',
        ],
    ]); ?>
</div>
