<?php

namespace app\modules\general\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\book\models\Book;

/**
 * BookSearch represents the model behind the search form of `app\modules\book\models\Book`.
 */
class BookSearch extends Book
{

    public $date_from;
    public $date_to;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'book_type', 'number'], 'integer'],
            [['create_date', 'FIO', 'purpose', 'doc', 'username'], 'safe'],
            [['summ'], 'number'],
            [['date_from', 'date_to'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'summ' => $this->summ,
            'book_type' => $this->book_type,
            'number' => $this->number,
        ]);

        $query->andFilterWhere(['like', 'FIO', $this->FIO])
            ->andFilterWhere(['like', 'purpose', $this->purpose])
            ->andFilterWhere(['like', 'doc', $this->doc])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['>=', 'create_date', $this->date_from ? strtotime($this->date_from . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'create_date', $this->date_to ? strtotime($this->date_to . ' 23:59:59') : null]);

        return $dataProvider;
    }

}
