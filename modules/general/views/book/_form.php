<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\book\models\Book;



/* @var $this yii\web\View */
/* @var $model app\modules\book\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

    <?
    $model->create_date = date('d.m.Y', $model->create_date);
    if ($model->book_type == 0) {
        $arrTypes = Book::getType();
    } else {
        $arrTypes = Book::getType2();
    }
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'create_date')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?= $form->field($model, 'summ')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FIO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'doc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList($arrTypes)->label('Тип платежа') ?>

    <?= $form->field($model, 'purpose')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
