<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\book\models\Book;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\general\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Общий вид');
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-12">
    <div class="col-lg-3 pull-right" >
        <div class="row">
            <a class="btn btn-success pull-right general_print" href="/">Печать</a>
        </div>
    </div>
</div>
<div class="book-index general">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'create_date',
                'value' => function ($model, $key, $index, $grid) {
                    return date('d.m.Y', $model->create_date);
                },
                'filter' => kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'attribute2' => 'date_to',
                    'type' => kartik\date\DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'weekStart' => 1, //неделя начинается с понедельника
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ],
                ]),
            ],
            'number',
            [
                'attribute' => 'book_type',
                'value' => function ($model, $key, $index, $grid) {
                    if ($model->book_type == 0) {
                        $text = 'Приход';
                    } else {
                        $text = 'Расход';
                    }
                    return $text;
                },
                'filter' => array('Приход', 'Расход'),
            ],
            'FIO',
            [
                'attribute' => 'type',
                'value' => function ($model, $key, $index, $grid) {
                    $arrTypes = Book::getTypes();
                    return $arrTypes[$model->type];
                },
                'filter' => Book::getTypes(),
            ],
            'purpose',
            'summ',
            'username',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ],
        ],
    ]); ?>
</div>
