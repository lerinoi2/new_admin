<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\book\models\Book;

/* @var $this yii\web\View */
/* @var $model app\modules\book\models\Book */
if ($model->book_type == 0) {
    $text = 'Приход';
} else {
    $text = 'Расход';
}

$this->title = $text;
\yii\web\YiiAsset::register($this);
?>
<div class="book-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number',
            [
                'attribute' => 'create_date',
                'value' => function ($model) {
                    return date('d.m.Y', $model->create_date);
                },
            ],
            'FIO',
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    $arrTypes = Book::getTypes();
                    return $arrTypes[$model->type];
                },
            ],
            'purpose',
            'doc',
            'summ',
            'username',
            [
                'attribute' => 'book_type',
                'value' => function ($model) {
                    if ($model->book_type == 0) {
                        $text = 'Приход';
                    } else {
                        $text = 'Расход';
                    }
                    return $text;
                },
            ],
        ],
    ]) ?>

    <?
    if ($model->book_type == 0) {
        ?>
        <a href="/edit/up/<?= $model->id ?>/print" class="btn btn-primary">Печать</a>
        <?
    } else {
        ?>
        <a class="btn btn-primary" href="/edit/down/<?= $model->id ?>/print">Печать</a>
        <?
    }
    ?>

</div>
