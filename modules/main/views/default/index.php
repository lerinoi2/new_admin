<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;

?>
<div class="site-index">
    <div class="jumbotron">
        <h2>Добро пожаловать, <?=Yii::$app->user->identity->username;?>!</h2>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-3 main-menu">
                <h2>Внести приход</h2>
                <div class="form-group">
                    <p><a class="btn btn-default" href="/receipt">Внести</a></p>
                </div>
            </div>
            <div class="col-lg-3 main-menu">
                <h2>Внести расход</h2>
                <div class="form-group">
                    <p><a class="btn btn-default" href="/expense">Внести</a></p>
                </div>
            </div>
            <div class="col-lg-3 main-menu">
                <h2>Общий вид</h2>
                <div class="form-group">
                    <p><a class="btn btn-default" href="/general/book">Просмотр</a></p>
                </div>
            </div>
            <div class="col-lg-3 main-menu">
                <h2>Кассовая книга</h2>
                <div class="form-group">
                    <p><a class="btn btn-default" href="/cash/book">Выбрать</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
