<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container">
        <?= Alert::widget() ?>
        <?php
        NavBar::begin([
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => array_filter([
                !Yii::$app->user->isGuest ?
                    ['label' => 'Главная', 'url' => ['/']] : [],
                !Yii::$app->user->isGuest ?
                    ['label' => 'Внести приход', 'url' => ['/receipt']] : [],
                !Yii::$app->user->isGuest ?
                    ['label' => 'Внести расход', 'url' => ['/expense']] : [],
                !Yii::$app->user->isGuest ?
                    ['label' => 'Общий вид', 'url' => ['/general/book']] : [],
                !Yii::$app->user->isGuest ?
                    ['label' => 'Кассовая книга', 'url' => ['/cash/book']] : [],
                Yii::$app->user->isGuest ?
                    ['label' => 'Вход', 'url' => ['/main/default/login']] :
                    ['label' => 'Выход',
                        'url' => ['/main/default/logout'],
                        'linkOptions' => ['data-method' => 'post']],
                !Yii::$app->user->isGuest ?
                    ['label' => 'Пересчитать все', 'url' => ['/recount']] : [],
            ]),
        ]);
        NavBar::end();
        ?>
        <?= $content ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
