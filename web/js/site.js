$(document).ready(function () {
    $(".book-index tbody tr").dblclick(function () {
        location.href = '/general/book/view/' + $(this).data('key');
    });

    if ($('.container').children().hasClass("cash")) {
        let date_from = $('.cash #booksearch-date_from').val().length,
            date_to = $('.cash #booksearch-date_to').val().length;
        if (date_from > 0) {
            $('.col-lg-12.cash-info-hide').removeClass('cash-info-hide');
            let arrKey = [];
            $('tbody tr').each(function (ind, el) {
                arrKey.push($(el).data('key'))
            });

            if (arrKey.length == 1) {
                $('.start-period').css('display', 'none');
                $('.sum-down-period').css('display', 'none');
                $('.sum-up-period').css('display', 'none');
                $('.end-period').css('display', 'none');
                $('.book_print').css('display', 'none');
            }

            $.get('/getCash', {
                start: $('.cash #booksearch-date_from').val(),
                end: $('.cash #booksearch-date_to').val(),
            }).done(function (data) {
                let arrRes = JSON.parse(data);

                $('.start-period').text(arrRes['start']);

                $('.sum-down-period').text(arrRes['down']);
                $('.sum-up-period').text(arrRes['up']);

                $('.end-period').text(arrRes['end']);
                let href = '/book_print?ids=' + JSON.stringify(arrKey) +
                    '&start=' + $('.cash #booksearch-date_from').val() +
                    '&end=' + $('.cash #booksearch-date_to').val() +
                    '&star_day=' + arrRes['start'] +
                    '&end_day=' + arrRes['end'];
                $('.book_print').attr('href', encodeURI(href));
            });
        }
    }

    $(".general_print").click(function (e) {
        e.preventDefault();
        let loc = decodeURI(location.search);
        location.href = "/general_print" + location.search
    })

});
